pub use super::worker::{Rc, WorkPtr, Worker};

use super::data::balance_changes;

pub struct BalanceChangeWork {
    work: WorkPtr,
}

impl Worker for BalanceChangeWork {
    fn new(base_worker: &WorkPtr) -> Self {
        BalanceChangeWork {
            work: Rc::clone(base_worker),
        }
    }
}

impl BalanceChangeWork {
    pub fn select(&mut self) {}
}
