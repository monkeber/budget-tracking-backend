pub use std::cell::RefCell;
pub use std::rc::Rc;

use sea_orm::*;
use std::sync::Once;
use futures::executor;

use super::worker;

const DATABASE_URL: &str =
    "postgres://bt_user:bt_password@localhost:5432/bt_database?sslmode=disable";

pub type DbClientPtr = Rc<RefCell<DatabaseTransaction>>;

pub struct ConnectionPool;

static mut POOL: DatabaseConnection = DatabaseConnection::Disconnected;
static INIT: Once = Once::new();

impl ConnectionPool {
    pub fn instance() -> ConnectionPool {
        unsafe {
            INIT.call_once(|| {
                POOL = executor::block_on(Database::connect(DATABASE_URL)).unwrap();
            });

            ConnectionPool
        }
    }

    pub fn acquire_worker(&mut self) -> Result<worker::WorkPtr, &'static str> {
        Err("Could not find a free connection")
    }

    pub fn release(&mut self, _conn_to_release: DbClientPtr) {
    }
}
