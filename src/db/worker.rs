pub use std::cell::RefCell;
pub use std::rc::Rc;

use sea_orm::*;
use futures::executor;

use super::connection;

pub struct Work {
    conn: connection::DbClientPtr,
}
pub type WorkPtr = Rc<RefCell<Work>>;

pub trait Worker {
    fn new(base_worker: &WorkPtr) -> Self;
}

impl Work {
    pub fn exec(&mut self, query: &str) -> Result<ExecResult, DbErr> {
        executor::block_on(self.conn.borrow_mut().execute_unprepared(query))
    }

    pub fn new(incoming_conn: connection::DbClientPtr) -> Self {
        Work {
            conn: incoming_conn,
        }
    }
}
