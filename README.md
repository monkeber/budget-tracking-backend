# Application For Managing Your Budget

The goal of the project is to create a budget managing app for tracking your expenses that will feature some specific functionality that I wasn't able to find in already existing apps.

Main feature is the ability to record a spending or some profit and "tag" it with several categories. In theory this will allow to build an extensive set of charts and statistics in general for tracking profits and expenses.

Other features may include:
- Tracking what funds are in cash and what are in the banking accounts.
- Handle several currencies, e.g. just an ability to change currency, handling several balances with different currencies, converting from one currency to another, etc.
- Allow different users for one instance of the server.
- Allow several users to use same budget.

Architecture wise I aim to split the project into two big parts - backend and frontend. Instead of having native apps for each of the platforms I use it's easier to have a deployed server somewhere and a web interface that will communicate with the server via some API.

## Tools And Technologies

### Database

For the finance related applications it's better to have a structured data - that is SQL based database. Good option is PostgreSQL.

### API

For the API I want to try using GraphQL instead of the usual REST.

### Backend Language

For the backend I want to try using Rust language to have some practice in it and since it seems to have matured enough to be able to find needed libraries for popular tools like GraphQL, Postgres, etc.

### Frontend Language

Haven't done much research on this yet, but my first though goes to TypeScript since it has most of the features of JS but has type checking sprinkled in.

## Build

The project should be easily built with the usual:
```bash
cargo build
```

### Generating DB Entities

Install sea-orm-cli with cargo. Run the following command:
```bash
sea-orm-cli generate entity -u "postgres://bt_user:bt_password@localhost:5432/bt_database?sslmode=disable" -o src/db/data
```

### Troubleshooting

1. You could run into issues during the build related to openssl package, in this case you probably just need to install `pkg-config`:
```bash
# Manjaro
sudo pacman -S pkg-config
# Ubuntu
sudo apt install libssl-dev
```
