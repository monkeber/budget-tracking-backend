use sea_orm::sea_query::QueryStatementBuilder;
use sea_orm::*;
use sea_query::{Alias, Expr, Query, SimpleExpr};

use sfb::db::data::*;

const DATABASE_URL: &str =
    "postgres://bt_user:bt_password@localhost:5432/bt_database?sslmode=disable";

#[tokio::test]
async fn db_test() -> Result<(), DbErr> {
    let db = Database::connect(DATABASE_URL).await?;
    let txn = db.begin().await?;

    let balance_columns: Vec<Alias> = [
        balance_changes::Column::Amount.as_str(),
        balance_changes::Column::CurrencyId.as_str(),
    ]
    .into_iter()
    .map(Alias::new)
    .collect();

    let select_column = (
        currencies::Entity,
        Alias::new(currencies::Column::Id.as_str()),
    );
    let sub_select = SimpleExpr::SubQuery(
        None,
        Box::new(
            Query::select()
                .column(select_column)
                .from(currencies::Entity)
                .and_where(Expr::col(currencies::Column::Name).eq("EUR".to_owned()))
                .to_owned()
                .into_sub_query_statement(),
        ),
    );

    let mut insert_stmt = Query::insert();
    insert_stmt
        .into_table(balance_changes::Entity)
        .columns(balance_columns)
        .values_panic([(5.0).into(), sub_select]);

    let builder = txn.get_database_backend();
    txn.execute(builder.build(&insert_stmt)).await?;

    txn.commit().await?;

    Ok(())
}
