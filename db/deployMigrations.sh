#!/usr/bin/env bash

scriptDir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

source ${scriptDir}/../docker/.user.env

dbmate --url ${DATABASE_URL} wait
dbmate --migrations-dir "${scriptDir}/../migrations" --url ${DATABASE_URL} --no-dump-schema up
