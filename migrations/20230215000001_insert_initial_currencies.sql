-- migrate:up

INSERT INTO public.currencies(name)
    VALUES
        ('EUR'),
        ('USD'),
        ('UAH'),
        ('GBP');

-- migrate:down

DELETE FROM public.currencies
    WHERE
        name IN ('EUR', 'USD', 'UAH', 'GBP');
