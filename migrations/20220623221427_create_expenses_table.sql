-- migrate:up

CREATE TABLE public.currencies (
    id SERIAL PRIMARY KEY,
    name TEXT UNIQUE NOT NULL
);

CREATE TABLE public.balance_changes (
    id SERIAL PRIMARY KEY,
    currency_id INT NOT NULL REFERENCES public.currencies(id),
    amount DOUBLE PRECISION NOT NULL,
    performed_at TIMESTAMPTZ NOT NULL DEFAULT NOW()
);

CREATE VIEW public.balance_changes_view AS
    SELECT
        bc.amount,
        cur.name,
        bc.performed_at
    FROM
        public.balance_changes bc
    JOIN
        public.currencies cur ON (bc.currency_id = cur.id);

-- migrate:down

DROP VIEW IF EXISTS public.balance_changes_view;
DROP TABLE IF EXISTS public.balance_changes;
DROP TABLE IF EXISTS public.currencies;
